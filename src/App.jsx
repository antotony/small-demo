import React, { useState } from "react";
import BaseField from "./components/BaseField";
import BaseForm from "./components/BaseForm"
import "./styles/App.css"

function App() {
    return (<div className="App">
        <BaseForm />

    </div>);
}

export default App;
