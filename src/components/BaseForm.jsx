import React, { useEffect, useState } from "react";
import BaseField from "./BaseField";


// Конфиг, id основного элемента и массив из id зависмых (в данном случае условие - notEmpty и свойство disabled)
const conditions1 = {
    "f1": ["f3"],
    "f2": ["f3"]
}



// const conditions2 = {
//     "f1": [],
//     "f2": ["f1"],
//     "f3": ["f1", "f2"]
// }

let conditions2 = {}

function transformConditions(conditions, ids){
    for (let id of ids){
        conditions2[id] = [];
        for (let condition in conditions){
            if (conditions[condition].find((val) => val == id)){
                conditions2[id].push(condition);
            }
        }
    }
}


//  Условия:
//  Первое поле - любой
//  Второе поле деактивируем, если первой пустое и наоборот
//  Третье поле деактивируем, если пустое второе или первое
//
//



const BaseForm = function(){

    const [val, setValue] = useState({  f1: {value: "", disabled: false},
                                        f2: {value: "", disabled: false},
                                        f3: {value: "", disabled: false}});

    useEffect(() => {
        transformConditions(conditions1, ["f1", "f2", "f3"]);
    }, [val])

    function setValueNormal(target){
        let obj = {...val}
        obj[target.id].value = target.value; 
        setValue(obj)
        validate(target.id, val)
    }

    function checkCondition(arr, val){
        let empty = false;
        for (let item of arr){
            empty = empty || val[item].value == ""
        }
        return empty;
    }
    
    function validate(id, val){
        let arr = conditions1[id];
        for (let item of arr){
            let disabled = checkCondition(conditions2[item], val);
            let obj = {...val}
            obj[item].disabled = disabled; 
            setValue(obj)
        }
    }

    


    return (
        <form>
            <BaseField id="f1" onChange={setValueNormal} value={val.f1.value} disabled={val.f1.disabled}/> 
            <BaseField id="f2" onChange={setValueNormal} value={val.f2.value} disabled={val.f2.disabled}/>
            <BaseField id="f3" onChange={setValueNormal} value={val.f3.value} disabled={val.f3.disabled}/>
        </form>
        
    )

}

export default BaseForm;