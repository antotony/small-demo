import React from "react";
import "../styles/BaseField.css"

const BaseField = function({id, value, onChange, disabled}){
    return (
        <div>
            <label>{id}</label>
            <input className="BaseField" disabled={disabled} id={id} value={value} onChange={(event) => onChange(event.target)}>
            </input>
        </div>
        
    )

}

export default BaseField;